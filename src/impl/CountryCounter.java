/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package impl;

import java.util.ArrayList;

/**
 * @author eflophi
 *
 */
public class CountryCounter {

    private int[][] worldMap;

    /**
     * counts the amount of "countries" (equal integers that are ADJACENT to each other) and returns the total in a given 2D array, if none are
     * adjacent they are counted as a single country
     * <p>
     * eg: {{1,2,2}, {2,3,3}} contains 4 countries
     *
     * @param worldMapInput
     *            - 2D array given by user.
     * @return - the amount of found countries
     */
    public int countryAmount(final int[][] worldMapInput) {
        worldMap = worldMapInput;
        int totalAmountOfCountries = 0;
        final ArrayList<int[]> knownIndexes = new ArrayList<>();

        for (int i = 0; i < worldMap.length; i++) {
            for (int j = 0; j < worldMap[i].length; j++) {
                if (!checkIndexInList(knownIndexes, i, j)) {
                    findAdjacentMatches(knownIndexes, i, j);
                    totalAmountOfCountries++;
                }
            }
        }
        return totalAmountOfCountries;
    }

    /**
     * checks if given integers appear in the array list of indices
     *
     * @param list
     *            List of indices (array of int[]) to check
     * @param row
     *            The row of the array
     * @param column
     *            The column of the array
     * @return true if the index is found
     */
    private boolean checkIndexInList(final ArrayList<int[]> list, final int row, final int column) {
        for (final int[] x : list) {
            if (x[0] == row && x[1] == column) {
                return true;
            }
        }
        return false;
    }

    /**
     * finds Integers of the same value that are adjacent to the given one and will continue recursively until all connected Integers of the same
     * value are found
     *
     * @param list
     *            The list of indexes for reference
     * @param row
     *            The location of of the starting points row
     * @param column
     *            The location of of the starting points column
     */
    private void findAdjacentMatches(final ArrayList<int[]> list, final int row, final int column) {
        list.add(new int[] { row, column });

        if (countryExistsAbove(row, column) && !checkIndexInList(list, row - 1, column)) {
            findAdjacentMatches(list, row - 1, column);
        }
        if (countryExistsBelow(row, column) && !checkIndexInList(list, row + 1, column)) {
            findAdjacentMatches(list, row + 1, column);
        }
        if (countryExistsToLeft(row, column) && !checkIndexInList(list, row, column - 1)) {
            findAdjacentMatches(list, row, column - 1);
        }
        if (countryExistsToRight(row, column) && !checkIndexInList(list, row, column + 1)) {
            findAdjacentMatches(list, row, column + 1);
        }
    }

    private boolean countryExistsAbove(final int row, final int column) {
        return isValidIndexAbove(row, column) && worldMap[row - 1][column] == worldMap[row][column];
    }

    private boolean countryExistsBelow(final int row, final int column) {
        return isValidIndexBelow(row, column) && worldMap[row + 1][column] == worldMap[row][column];
    }

    private boolean countryExistsToLeft(final int row, final int column) {
        return (column != 0) && (worldMap[row][column - 1] == worldMap[row][column]);
    }

    private boolean countryExistsToRight(final int row, final int column) {
        return (column != worldMap[row].length - 1) && (worldMap[row][column + 1] == worldMap[row][column]);
    }

    private boolean isValidIndexAbove(final int row, final int column) {
        return row != 0 && (column + 1) <= worldMap[row - 1].length;
    }

    private boolean isValidIndexBelow(final int row, final int column) {
        return row != worldMap.length - 1 && (column + 1) <= worldMap[row + 1].length;
    }
}
