/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import impl.CountryCounter;

/**
 * @author eflophi
 *
 */
public class CountryCounterTest {
    CountryCounter countries = new CountryCounter();

    @Test
    public void testCountryCounter() {
        assertEquals(2, countries.countryAmount(new int[][] { { 1, 2 }, { 2, 2 } }));
    }

    @Test
    public void testCountryCounterWithAllIndividualCountries() {
        assertEquals(6, countries.countryAmount(new int[][] { { 1, 2, 3 }, { 4, 5, 6 } }));
        assertEquals(4, countries.countryAmount(new int[][] { { 1, 2 }, { 4, 5 } }));
    }

    @Test
    public void testCountryCounterWithWideArray() {
        assertEquals(10, countries.countryAmount(new int[][] { { 1, 2, 2, 3, 4, 4, 5 }, { 6, 6, 5, 3, 4, 5, 6 }, { 1, 6, 6, 3, 4, 4, 4 } }));
    }

    @Test
    public void testCountryCounterWithTallArray() {
        assertEquals(9,
                countries.countryAmount(new int[][] { { 1, 2 }, { 1, 3 }, { 1, 3 }, { 3, 4 }, { 5, 5 }, { 4, 4 }, { 4, 2 }, { 4, 4 }, { 6, 4 }, }));
    }

    @Test
    public void testCountryCounterWithLargeArray() {
        assertEquals(11,
                countries.countryAmount(new int[][] { { 5, 4, 4 }, { 4, 3, 4 }, { 3, 2, 4 }, { 2, 2, 2 }, { 3, 3, 4 }, { 1, 4, 4 }, { 4, 1, 1 } }));
    }

    @Test
    public void testCountryCounterWithOddArray() {
        assertEquals(3, countries.countryAmount(new int[][] { { 5, 4, 4 }, { 3, 4 } }));
        assertEquals(12, countries.countryAmount(new int[][] { { 5, 4, 4 }, { 4, 3 }, { 3, 2, 4 }, { 2, 2 }, { 3, 3, 4 }, { 1, 4 }, { 2 } }));
        assertEquals(5,
                countries.countryAmount(new int[][] { { 1, 1, 1, 1, 1, 1 }, { 1 }, { 1, 2 }, { 1, 2, 3 }, { 2, 2, 4, 4 }, { 5, 5, 4, 4, 4 } }));
    }

}
